const { fighter } = require('../models/fighter');
const {checkKeys,checkBody} = require('./keys.validator.middleware');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    validateFighter(req, res);
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    validateFighter(req, res);
    next();
}

function validateFighter(req, res){

    checkBody(res,req);
    if(req.isValid!==false){
        const newFighter = req.body;
        const powerNum = parseFloat(newFighter.power);
        if(typeof powerNum !=='number'||powerNum>100){
            res.status(400).send({
                error:true,
                message:'Bad request. Rule power - number and must be less than < 100'
            });
            req.isValid=false;
            return;
        }
    
        if(Object.keys(newFighter).includes('id')){
            res.status(400).send({
                error:true,
                message:`Request is not valid. Cannot provide id`
            });
            req.isValid=false;
            return;
        }
        checkKeys(res,fighter,newFighter);
    }
    



}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;