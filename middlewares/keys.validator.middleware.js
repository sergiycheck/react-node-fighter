
const checkBody=(res,req)=>{
    if(req.body==undefined){
        res.status(400).send({
            error:true,
            message:`body is undefined`
        });
        req.isValid = false;
        return;
    }
    

    if(Object.keys(req.body).length<0){
        res.status(400).send({
            error:true,
            message:`There must be properties`
        });
        req.isValid = false;
        return;
    }
}


const checkKeys=(res,item,itemToCheck)=>{

    let validFields = Object.keys(item).filter(key=>key!=='id');
    let newItemFields = Object.keys(itemToCheck);
    let isTypeCorrect=(
        validFields.every(key=>newItemFields.includes(key))&&
        validFields.length===newItemFields.length
    );
    if(!isTypeCorrect){
        res.status(400).send({
            error:true,
            message:`Bad request. Cannot add new fields`,itemToCheck
        });
        req.isValid = false;
        return;
    }
}

exports.checkKeys = checkKeys
exports.checkBody = checkBody