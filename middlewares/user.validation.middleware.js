const { user } = require('../models/user');
const {checkKeys,checkBody} = require('./keys.validator.middleware');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    userValidation(req, res);
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    userValidation(req, res);
    next();
}

function userValidation(req, res){

    checkBody(res,req);
    if(req.isValid!==false){

        const newUser = req.body;
        const emailRegex = /\w+@gmail\.com$/;
        const phoneRegex = /\+380\d{9}$/;
        
        const emailValid = emailRegex.test(newUser.email);
        if(!emailValid){
            res.status(400).send({
                error:true,
                message:`${newUser.email} is not valid. Rule: email - only gmail`
            });
            req.isValid = false;
            return;
        }
        const phoneValid = phoneRegex.test(newUser.phoneNumber);
        if(!phoneValid){
            res.status(400).send({
                error:true,
                message:`${newUser.phoneNumber} is not valid. Rule: phoneNumber: +380xxxxxxxxx`
            });
            req.isValid = false;
            return;
        }
        if(newUser.password.length<=3){
            res.status(400).send({
                error:true,
                message:`${newUser.password} is not valid. Min length 3`
            });
            req.isValid = false;
            return;
            
        }
        if(Object.keys(newUser).includes('id')){
            res.status(400).send({
                error:true,
                message:`Request is not valid. Cannot provide id`
            });
            req.isValid = false;
            return;
        }
        checkKeys(res,user,newUser);

    }

}



exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;