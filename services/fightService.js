const { FightRepository } = require('../repositories/fightRepository');
const {BaseService} = require('./baseService');

class FightersService extends BaseService {
    constructor(){
        super(FightRepository);
    }
    // OPTIONAL TODO: Implement methods to work with fights
}

module.exports = new FightersService();