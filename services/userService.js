const { UserRepository } = require('../repositories/userRepository');
const {BaseService} = require('./baseService');

class UserService extends BaseService {
    constructor(){
        super(UserRepository);
    }

    // TODO: Implement methods to work with user

}

module.exports = new UserService();