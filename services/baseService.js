
class BaseService{
    constructor(repository){
        this.repository = repository;
    }
    verify(data){
        return !data?null:data;
    }

    add(item){
        const newItem = this.repository.create(item);
        return (this.verify(newItem));
    }

    getAll(){
        const items = this.repository.getAll();
        return(this.verify(items));
    }

    search(search) {
        const item = this.repository.getOne(item=>item.id==search);
        return (this.verify(item));
    }
    update(id,item){
        const updatedResult = this.repository.update(id,item);
        return (this.verify(updatedResult));
    }
    delete(id){
       const deleteResult = this.repository.delete(id);
       return (this.verify(deleteResult)); 
    }
}
exports.BaseService = BaseService