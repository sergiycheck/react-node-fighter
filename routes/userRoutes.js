const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { route } = require('./authRoutes');

const router = Router();

// TODO: Implement route controllers for user

//USER:
// GET /api/users
// GET /api/users/:id
// POST /api/users
// PUT /api/users/:id
// DELETE /api/users/:id

router.get('/',(req,res,next)=>{
    const users = UserService.getAll();

    if(users){
        res.send(users);
    }else{
        res.status(404).send({
            error:true,
            message:'Users not found'
        });
        
    }
});

router.get('/:id',(req,res,next)=>{
    const id = req.params.id
    if(id){
        const user = UserService.search(id);

        if(user){
            res.status(200).send(user);
        }else{
            res.status(404).send({
                error:true,
                message:`User with id: ${id} not found`
            });

        }
    }else{
        res.status(400).send({
            error:true,
            message:'Bad request'
        });  
    }
})

//add middleware
router.post('/',createUserValid,(req,res,next)=>{

    if(req.isValid!==false){
        const newUserJson = req.body;
        const newUser = UserService.add(newUserJson);
    
        if(newUser){
            res.status(200).send(newUser);
        }else{
            res.status(404).send({
                error:true,
                message:'Error occurred creating user '
            });
        }
    }

})

router.put('/:id',updateUserValid,(req,res,next)=>{
    if(req.isValid!==false){
        const id = req.params.id
        if(id){
            const userToUpdate = req.body;
            const updatedUser = UserService.update(userToUpdate);

            if(updatedUser){
                res.status(200).send(updatedUser);
            }else{
                res.status(404).send({
                    error:true,
                    message:'Error occurred updating user'
                });
            }
        }else{

            res.status(400).send({
                error:true,
                message:'Bad request'
            });  
        }
    }
})

router.delete('/:id',(req,res,next)=>{
    const id = req.params.id
    if(id){
        const deleteRes = UserService.delete(id);

        if(deleteRes){
            res.status(200).send(deleteRes);
        }else{
            res.status(404).send({
                error:true,
                message:'Error occurred deleting user'
            });
        }
    }else{

        res.status(400).send({
            error:true,
            message:'Bad request'
        });  
    }
})


module.exports = router;