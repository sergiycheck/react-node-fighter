const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/',(req,res,next)=>{
    const fighters = FighterService.getAll();

    if(fighters){
        res.send(fighters);
    }else{
        res.status(404).send({
            error:true,
            message:'fighters not found'
        });
        
    }
});

router.get('/:id',(req,res,next)=>{
    const id = req.params.id;
    if(id){
        const fighter = FighterService.search(id);

        if(fighter){
            res.status(200).send(fighter);
        }else{
            res.status(404).send({
                error:true,
                message:`fighter with id: ${id} not found`
            });

        }
    }else{
        res.status(400).send({
            error:true,
            message:'Bad request'
        });  
    }
})

//add middleware
router.post('/',createFighterValid,(req,res,next)=>{
    if(req.isValid!==false){
        const newFighterJson = req.body;
        const newFighter = FighterService.add(newFighterJson);

        if(newFighter){
            res.status(200).send(newFighter);
        }else{
            res.status(404).send({
                error:true,
                message:'Error occurred creating fighter'
            });
        }
    }
})

router.put('/:id',updateFighterValid,(req,res,next)=>{
    if(req.isValid!==false){
        const id = req.params.id;
        if(id){
            const fighterToUpdate = req.body;
            const updatedFighter = FighterService.update(fighterToUpdate);

            if(updatedFighter){
                res.status(200).send(updatedFighter);
            }else{
                res.status(404).send({
                    error:true,
                    message:'Error occurred updating fighter'
                });
            }
        }else{
            
            res.status(400).send({
                error:true,
                message:'Bad request'
            });  
        }
    }
})

router.delete('/:id',(req,res,next)=>{
    const id = req.params.id;
    if(id){
        const deleteRes = FighterService.delete(id);

        if(deleteRes){
            res.status(200).send(deleteRes);
        }else{
            res.status(404).send({
                error:true,
                message:'Error occurred deleting fighter'
            });
        }
    }else{

        res.status(400).send({
            error:true,
            message:'Bad request'
        });  
    }
})




module.exports = router;